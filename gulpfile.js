'use strict'

const gulp = require('gulp')
const sass = require('gulp-sass')
const less = require('gulp-less')
const concat = require('gulp-concat')
const changed = require('gulp-changed')
const filelog = require('gulp-filelog')
const clean = require('gulp-rimraf')
const addsrc = require('gulp-add-src')
const merge = require('merge-stream')
const minify = require('gulp-minify')

gulp.task('build.css', function () {
  let src = 'browser/css/**/*.sass'
  let dest = 'app/statics/css'

  return gulp.src(src)
    .pipe(sass())
    .pipe(concat('app.css'))
    .pipe(filelog('build.css'))
    .pipe(gulp.dest(dest))
})

gulp.task('build.js', function () {
  let src = [
    'browser/js/shared/app.js',
    'browser/js/**/*.js'
  ]
  let dest = 'app/statics/js'

  return gulp.src(src)
    .pipe(concat('app.js'))
    .pipe(filelog('build.js'))
    .pipe(gulp.dest(dest))
})

gulp.task('build.css_vendor', function () {
  let src = [
    'node_modules/semantic-ui-css/semantic.css'
  ]
  let dest = 'app/statics/css'

  return gulp.src(src)
    .pipe(concat('vendor.css'))
    .pipe(filelog('build.css_vendor'))
    .pipe(gulp.dest(dest))
})

gulp.task('build.js_vendor', function () {
  let src = [
    // Essentials
    'node_modules/jquery/dist/jquery.js',
    'node_modules/underscore/underscore.js',
    // Angular
    'node_modules/angular/angular.js',
    'node_modules/angular-ui-router/release/angular-ui-router.js',
    'node_modules/angular-animate/angular-animate.js',
    'node_modules/angular-touch/angular-touch.js',
    'node_modules/restangular/dist/restangular.js',
    // Design
    'node_modules/semantic-ui-css/semantic.js'
  ]
  let dest = 'app/statics/js'

  return gulp.src(src)
    .pipe(concat('vendor.js'))
    .pipe(minify({ext:{min:'.js'}}))
    .pipe(filelog('build.js_vendor'))
    .pipe(gulp.dest(dest))
})

gulp.task('build.font_vendor', function () {
  let src = [
    'node_modules/semantic-ui-css/themes/default/assets/fonts/*.*'
  ]
  let dest = 'app/statics/css/themes/default/assets/fonts/'

  return gulp.src(src)
    .pipe(filelog('build.font_vendor'))
    .pipe(gulp.dest(dest))
})

gulp.task('watch', function () {
  var watcher = gulp.watch('browser/**', ['build'])
  watcher.on('changed', function (event) {
    console.log('File ' + event.path + ' was ' + event.type)
  })
})

gulp.task('build', ['build.css', 'build.js', 'build.css_vendor', 'build.js_vendor', 'build.font_vendor'])
gulp.task('default', ['build', 'watch'])
