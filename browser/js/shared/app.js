'use strict';

angular.module('app', ['ui.router', 'restangular'])
    .run(['$rootScope', '$state', '$http', 'Restangular',
        function ($rootScope, $state, $http, Restangular) {
            Restangular.setBaseUrl('/v1')

            $http({
                method: 'GET',
                url: 'session'
            }).then(function (res) {
                $rootScope.session = res.data.phon
            })

            $state.go('main');
        }
    ])