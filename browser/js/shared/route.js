angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'main'
      })
      .state('debug', {
        url: '/debug',
        templateUrl: 'debug'
      })
      .state('com', {
        url: '/com',
        templateUrl: 'communicator'
      })
  }])