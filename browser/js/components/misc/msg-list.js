angular.module('app')
  .directive('ccMsgList', ['$interval',
    function ($interval) {
      return {
        scope: {
          list: '=ccMsgList',
          active: '=active'
        },
        templateUrl: 'msg-list-directive',
        link: function (scope, element) {
          scope.$watchCollection('list', function () {
            if (scope.active !== true) { return }
            var children = element[0].children
            if (children.length > 0) {
              var c = children[children.length - 1]
              $interval(function () {
                c.scrollIntoView()
              }, 0, 1)
            }
          })
        }
      }
    }
  ])