angular.module('app')
  .controller('debugController', ['$rootScope', '$scope', '$interval', 'Restangular',
    function ($rootScope, $scope, $interval, Restangular) {
      var SessionMessages = Restangular.allUrl('msgs', 'v1/msgs/session')
      var lastMsg = new Date('1970')
      $scope.session = $rootScope.session
      $scope.messages = []
      $scope.autoscroll = true

      $scope.cleanMsgs = function () {
        $scope.messages.length = 0
      }

      $scope.toggleAutoscroll = function () {
        $scope.autoscroll = !$scope.autoscroll
      }

      $scope.refresh = function () { 
        SessionMessages.getList({ createdAt: lastMsg }).then(function (messages) {
          if (messages.length > 0) {
            messages.forEach((v) => {
              $scope.messages.push( v )
            })

            lastMsg = new Date(messages[messages.length-1].createdAt)
          }
        })
      }

      var prom = $interval($scope.refresh, 1000)
      
      $scope.$on("$destroy", () => {
        $interval.cancel(prom)
      })
      
      // Debug
      $scope.cancel = function () { $interval.cancel(prom) }
    }
  ])