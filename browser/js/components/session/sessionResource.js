/**
 * Created by MechWipf on 28.08.2016
 */

angular.module('app')
  .factory('Session', ['$http',
    function ($http) {
      var session = null

      return function () {
        if (session != null) {
          $http({
            method: 'GET',
            url: 'cc/session'
          }).then(function () {
            
          })
        }
      }
    }])