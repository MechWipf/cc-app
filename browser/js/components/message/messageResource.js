/**
 * Created by MechWipf on 01.09.2016
 */

angular.module('app')
  .factory('Messages', ['Restangular',
    function (Restangular) {
      return Restangular.service('msgs')
    }
  ])