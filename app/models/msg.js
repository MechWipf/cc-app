'use strict'

const mongoose = require('mongoose')

let msgSchema = mongoose.Schema({
  _sender: { type: String, required: true, ref: 'device' },
  targetPhon: { type: String, required: true },
  content: { type: String, required: true },
  createdAt: { type: Date, expires: 60*30, default: Date.now }
})

msgSchema.set('toJSON', { getters: true, virtuals: false })

module.exports = mongoose.model('msg', msgSchema)
