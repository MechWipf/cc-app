'use strict'

const mongoose = require('mongoose')

let deviceSchema = mongoose.Schema({
  uuid: { type: String, required: true, unique: true },
  phon: { type: String, required: true, unique: true }
}, {
  timestamps: true
})

deviceSchema.set('toJSON', { getters: true, virtuals: false })

module.exports = mongoose.model('device', deviceSchema)
