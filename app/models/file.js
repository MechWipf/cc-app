'use strict'

const mongoose = require('mongoose')

let fileSchema = mongoose.Schema({
  name: { type: String, required: true },
  content: { type: String, required: true }
})

fileSchema.set('toJSON', { getters: true, virtuals: false })

module.exports = mongoose.model('file', fileSchema)
