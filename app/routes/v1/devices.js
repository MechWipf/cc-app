'use strict'

const express = require('express')
const Device = require('../../models/device')
const bodyParser = require('body-parser')
const uuid = require('uuid')
const phonetic = require('phonetic')

let router = express.Router()
router.use(bodyParser.json())

router.route('/')
  .get(function (req, res) {
    Device.find((err, devices) => {
      if (err) return console.error(err)

      devices = devices.map((device) => {
        let tmp = device.toJSON()
        // tmp.id = tmp._id
        delete tmp.__v
        delete tmp._id
        return tmp
      })
      res.send(devices)
    })
  })
  .post(function (req, res) {
    let input = req.body
    let _uuid = uuid.v4()
    
    let device = new Device({
      uuid: _uuid,
      phon: phonetic.generate({
        seed: _uuid,
        syllables: 4,
        capFirst: false
      })
    })

    device.save()
    let err = device.validateSync()

    if (err) {
      res.status(400).send(err)
    } else {
      res.send(device.toJSON())
    }
  })

module.exports = router