'use strict'

const express = require('express')
const Msg = require('../../models/msg')
const Device = require('../../models/device')
const bodyParser = require('body-parser')

let router = express.Router()

router.use((req, res, next) => {
  delete req.headers['content-encoding']
  next()
})
router.use(bodyParser.json({ inflate: true, type: '*/*' }))

router.route('/')
  .get(function (req, res) {
    Msg.find((err, msgs) => {
      if (err) return console.error(err)

      msgs = msgs.map((msg) => {
        let tmp = msg.toJSON()
        tmp.id = tmp._id
        delete tmp.__v
        delete tmp._id
        return tmp
      })
      res.send(msgs)
    })
  })
  .post(function (req, res) {
    let input = req.body

    Device.findOne({ uuid: input.sourceId }, function (err, device) {
      if (device) {
        let msg = new Msg({
          _sender: device.id,
          targetPhon: input.targetPhon,
          content: input.content
        })

        msg.save()
        msg.validate(function (err, msg) {
          if (err) {
            res.status(400).send(err)
          } else if (msg) {
            res.send(msg.toJSON())
          } else {
            res.status(200).send('ok')
          }
        })
      } else {
        res.status(400).send('Unknown device.')
      }
    })
  })

router.route('/session')
  .get(function (req, res) {
    let phon = req.session.phon
    let conCreatedAt = req.query.createdAt || new Date("1970")
    
    Msg
      .find({ targetPhon: phon, createdAt: { $gt: conCreatedAt } }, 'updatedAt createdAt content _id _sender')
      .populate('_sender', 'phon')
      .sort({ createdAt: 1 })
      .exec((err, msgs) => {
        if (err) {
          res.status(400).send(err)
        } else if (msgs) {
          res.send(msgs)
        } else {
          res.status(200).send([])
        }
      })
  })

module.exports = router