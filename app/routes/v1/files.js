'use strict'

const express = require('express')
const File = require('../../models/file')
const bodyParser = require('body-parser')

let router = express.Router()
router.use(bodyParser.json())

router.route('/')
  .get(function (req, res) {
    File.find((err, files) => {
      if (err) return console.error(err)

      files = files.map((file) => {
        let tmp = file.toJSON()
        tmp.id = tmp._id
        delete tmp.__v
        delete tmp._id
        return tmp
      })
      res.send(files)
    })
  })
  .post(function (req, res) {
    let input = req.body

    let file = new File({
      name: input.name,
      content: input.content
    })

    file.save()
    let err = file.validateSync()

    if (err) {
      res.status(400).send(err)
    } else {
      res.send(file.toJSON())
    }
  })

router.route('/:file_id')
  .get(function (req, res) {
    let file_id = req.params.file_id ? req.params.file_id : 0

    File.findById(file_id, function (err, found) {
      if (err) {
        res.status(400).send(err)
      } else {
        let tmp = found.toJSON()
        tmp.id = tmp._id
        delete tmp.__v
        delete tmp._id
        res.send(tmp)
      }
    })
  })

module.exports = router
