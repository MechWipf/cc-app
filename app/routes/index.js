'use strict'

const express = require('express')
const fs = require('fs')
const phonetic = require('phonetic')

let router = express.Router()

router.get('/', function (req, res) {
  let templates = []

  let files = fs.readdirSync('app/views/templates/')
  files.forEach(function (filePath) {
    res.render('templates/' + filePath, {}, function (err, html) {
      if (err) {
        console.log('Template ' + filePath + ' could not be loaded: \n' + err)
      } else {
        templates.push({id: filePath.replace(/\..+?$/, ''), content: html})
      }
    })
  })
  
  if (req.session.phon == null) {
    req.session.phon = phonetic.generate({
      seed: req.session.cookie.id,
      capFirst: false
    })
  }

  res.render('index', { templates: templates })
})

router.get('/session', function (req, res) {
  if (req.session.phon == null) {
    req.session.phon = phonetic.generate({
      seed: req.session.cookie.id,
      capFirst: false
    })
  }

  res.send({
    phon: req.session.phon
  })
})

module.exports = router
