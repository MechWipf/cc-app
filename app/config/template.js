'use strict'

module.exports = {
  IP: "[::]",
  Port: 80,
  Database: "mongodb://name:passwd@host/db",
  CookieSecret: "a-secret-key"
}
