#!/bin/env node
// OpenShift sample Node application
'use strict'
const express = require('express')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const mongoose = require('mongoose')
const compression = require('compression')

let MyApp = function () {
  let self = {}

  self.initRoutes = function () {
    let app = self.app
    
    // other stuff
    app.use('/cc', express.static(__dirname + '/statics'))

    // RESTful
    app.use('/cc/v1/files', require('./routes/v1/files'))
    app.use('/cc/v1/msgs', require('./routes/v1/msgs'))
    app.use('/cc/v1/devices', require('./routes/v1/devices'))

    // main site
    app.use('/cc', require('./routes/index'))
  }

  self.setup = function () {
    let env = process.env.NODE_ENV || 'development'
    let config = require(__dirname + '/config/' + env)

    console.log('Running in ' + env + ' mode.')

    self.ipaddress = config.IP
    self.port      = config.Port
    self.db        = config.Database
    self.secret    = config.CookieSecret
  }

  self.initialize = function () {
    self.app = express()
    
    // render engine
    self.app.set('view engine', 'jade')
    self.app.set('views', __dirname + '/views')

    // compression
    self.app.use(compression({
      filter: (req, res) => {
        if (req.headers['x-no-compression']) {
          return false
        }

        return compression.filter(req, res)
    }}))

    mongoose.Promise = global.Promise
    self.connectDB()
      .on('error', console.log)
      .on('disconnect', self.connectDB)
      .on('open', () => {
        console.log('Connected to DB.')
      })

    // session handling
    self.app.use(session({
      resave: true,
      saveUninitialized: true,
      secret: self.secret,
      cookie: { maxAge: 60000 * 60 * 4 },
      store: new MongoStore({
        mongooseConnection: mongoose.connection
      })
    }))

    self.initRoutes()
  }

  self.connectDB = function () {
    let options = { server: { socketOptions: { keepAlive: 1 } } }
    console.log('Connecting to DB.')
    return mongoose.connect(self.db, options).connection
  }

  self.listen = function () {
    self.app.listen(self.port, self.ipaddress, function () {
      console.log('%s: Node server started on %s:%d ...', Date(Date.now()), self.ipaddress, self.port)
    })
  }

  self.start = function () {
    self.listen()
  }

  return self
}

// Create MyApp, run all initialization work and start the app
let zapp = MyApp()
zapp.setup()
zapp.initialize()
zapp.start()
